const { SSMClient, GetParameterCommand } = require("@aws-sdk/client-ssm");

const run = async (context, Name, region) => {
   try {
     const client = new SSMClient({
       region
     });
     const command = new GetParameterCommand({Name});
     const response = await client.send(command);
     return response.Parameter.Value
   }
   catch (e) {
     throw Error(e.message)
   }
}

module.exports.templateTags = [{
    name: 'aws_ssm_param',
    displayName: 'AWS SSM Parameter',
    description: 'AWS SSM Parameter',
    args: [
        {
            displayName: 'Parameter name',
            type: 'string'
        },
        {
            displayName: 'Region',
            type: 'string',
            defaultValue: 'us-east-1'
        }
    ],
    run
}];
